# 网页登录框动画效果的实现

#### Description
见过很多动图，大都叫“攻城尸追着砍系列”。因此，作为一具“射鸡尸”，要想不被追着砍，还是自己来实现一些动效吧！主要动态效果包括：“添加”按钮和对话框颜色的变化效果，还有“提交”按钮的“抖动”效果。

实现的时候动画是用的CSS3的Animation，因此要能显示效果必须使用“真正的”浏览器。（老旧的IE系列是不行的）可正常显示的最新版浏览器包括但不限于：Safari、Firefox、Microsoft Edge。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
