# 网页登录框动画效果的实现

#### 介绍
见过很多动图，大都叫“攻城尸追着砍系列”。因此，作为一具“射鸡尸”，要想不被追着砍，还是自己来实现一些动效吧！主要动态效果包括：“添加”按钮和对话框颜色的变化效果，还有“提交”按钮的“抖动”效果。

实现的时候动画是用的CSS3的Animation，因此要能显示效果必须使用“真正的”浏览器。（老旧的IE系列是不行的）可正常显示的最新版浏览器包括但不限于：Safari、Firefox、Microsoft Edge。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
